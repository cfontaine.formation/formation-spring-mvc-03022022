package fr.dawan.springcore;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.dawan.springcore.beans.Client;
import fr.dawan.springcore.beans.Contact;
import fr.dawan.springcore.beans.Formation;

public class MainXML {

    public static void main(String[] args) {
        // Création du conteneur
        ApplicationContext ctx = new ClassPathXmlApplicationContext("beans.xml"); // new String[] { "beans.xml" ,"adresse.xml"}
        System.out.println("---------------------------------------------------");

        // getBean permet de récupérer les instances des beans depuis le conteneur
        Formation f1 = ctx.getBean("formation1", Formation.class);
        System.out.println("Bean formation1: " + f1);

        Formation f2 = ctx.getBean("formation2", Formation.class);
        System.out.println("Bean formation2: " + f2);

        Formation f3 = ctx.getBean("formationWord", Formation.class);
        System.out.println(f3);

        // Porté (Scope)

        //- Singleton:
        // Pour un singleton (ex: formation 1), getBean("formation1") retourne toujours
        // la même instance
        Formation f11 = ctx.getBean("formation1", Formation.class);
        System.out.println(f1);
        System.out.println(f11);

        //- Prototype:
        // Pour un prototype (ex: formation2) getBean("formationWord") retourne une
        // nouvelle instance à chaque appel
        // correspond à un new
        Formation f12 = ctx.getBean("formation2", Formation.class);
        System.out.println(f12);

        // Exercice Création de Bean
    //    Adresse adr1 = ctx.getBean("adresse1", Adresse.class);
    //    System.out.println(adr1);
//        Adresse adr2=ctx.getBean("adresse2", Adresse.class);
//        System.out.println(adr2);

        // Injection de dépendence explicite
//        Contact c1 = ctx.getBean("contact1", Contact.class);
//        System.out.println(c1);
//        Contact c2 = ctx.getBean("contact2", Contact.class);
//        System.out.println(c2);
//        Contact c3 = ctx.getBean("contact3", Contact.class);
//        System.out.println(c3);

        // Injection de dépendence implicite
        // - par type
//      Contact c4=ctx.getBean("contact4",Contact.class);
//      System.out.println(c4);

        // - par nom
//      Contact c5=ctx.getBean("contact5",Contact.class);
//      System.out.println(c5);
    
        // - avec le constructeur
        Contact c6 = ctx.getBean("contact6", Contact.class);
        System.out.println(c6);
        
        Formation f4=ctx.getBean("formation4",Formation.class);
        System.out.println(f4);
        
        
        // Héritage
        Client cl1=ctx.getBean("client1",Client.class);
        System.out.println(cl1);
        
//        Contact c7 = ctx.getBean("contact7",Contact.class);
//        System.out.println(c7);
        
        Client cl2=ctx.getBean("client2",Client.class);
        System.out.println(cl2);
        
        Client cl3=ctx.getBean("client3",Client.class);
        System.out.println(cl3);
        
        Contact c8 = ctx.getBean("contact8", Contact.class);
        System.out.println(c8);
        
        Client cl4=ctx.getBean("client4",Client.class);
        System.out.println(cl4);
        
        // Fermer le conteneur
        ((AbstractApplicationContext) ctx).close();
    }

    
}
