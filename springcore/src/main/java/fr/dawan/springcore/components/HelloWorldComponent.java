package fr.dawan.springcore.components;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component(value = "helloworld")
@Scope("prototype")
@PropertySource("classpath:config.properties")
public class HelloWorldComponent {
    
    @Value("${bddurl}")
    private String data;

    
    
    public HelloWorldComponent() {
    }



    public HelloWorldComponent(String data) {
        this.data = data;
    }



    public String getData() {
        return data;
    }



    public void setData(String data) {
        this.data = data;
    }

    public void afficher() {
        System.out.println("Hello world");
    }



    @Override
    public String toString() {
        return "HelloWorldComponent [data=" + data + ", toString()=" + super.toString() + "]";
    }
    


    
    

}
