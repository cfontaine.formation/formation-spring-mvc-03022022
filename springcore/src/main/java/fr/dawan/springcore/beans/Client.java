package fr.dawan.springcore.beans;

public class Client extends Contact {

    private static final long serialVersionUID = 1L;

    private String numeroClient;

    public Client() {
    }

    public Client(String prenom, String nom, Adresse adresse,String numeroClient) {
        super(prenom, nom, adresse);
        this.numeroClient=numeroClient;
    }

    public String getNumeroClient() {
        return numeroClient;
    }

    public void setNumeroClient(String numeroClient) {
        this.numeroClient = numeroClient;
    }

    @Override
    public String toString() {
        return "Client [numeroClient=" + numeroClient + ", toString()=" + super.toString() + "]";
    }

}
