package fr.dawan.springcore.beans;

import java.io.Serializable;

public class Formation implements Serializable {

    private static final long serialVersionUID = 1L;

    private String intitule;
    
    private int duree;
    
    private double prix;
    
    private String description;

    public Formation() {
        System.out.println("Constructeur par défaut");
    }

    public Formation(String intitule, int duree, double prix, String description) {
        System.out.println("Constructeur 4 paramètres");
        this.intitule = intitule;
        this.duree = duree;
        this.prix = prix;
        this.description = description;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        System.out.println("Setter: intitule");
        this.intitule = intitule;
    }

    public int getDuree() {
        return duree;
    }

    public void setDuree(int duree) {
        System.out.println("Setter: duree");
        this.duree = duree;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        System.out.println("Setter: prix");
        this.prix = prix;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        System.out.println("Setter: description");
        this.description = description;
    }

    @Override
    public String toString() {
        return "Formation [intitule=" + intitule + ", duree=" + duree + ", prix=" + prix + ", description="
                + description + ", toString()=" + super.toString() + "]";
    }
    
    public void init() {
        System.out.println("Méthode init");
    }

    public void destroy() {
        System.out.println("Méthode destroy");
    }
}
