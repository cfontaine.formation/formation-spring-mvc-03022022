 package fr.dawan.springcore.beans;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.beans.factory.annotation.Qualifier;

public class Contact implements Serializable {

    private static final long serialVersionUID = 1L;

    private String prenom;

    private String nom;

   // @Autowired // (required = false)
   // @Qualifier("adresse2")
   // @Resource//(name="adresse1")
   // @Inject
   // @Named(value =  "adresse2")
    private Adresse adresse;
    
    private List<Telephone> telephones;

    public Contact() {

    }

   // @Autowired
    public Contact(String prenom, String nom, @Qualifier("adresse2") Adresse adresse) {
        this.prenom = prenom;
        this.nom = nom;
        this.adresse = adresse;
    }

    
    
    public Contact(String prenom, String nom, Adresse adresse, List<Telephone> telephones) {
        this.prenom = prenom;
        this.nom = nom;
        this.adresse = adresse;
        this.telephones = telephones;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Adresse getAdresse() {
        return adresse;
    }

   // @Autowired
    public void setAdresse(/*@Qualifier("adresse1")*/ Adresse adresse) {
        this.adresse = adresse;
    }

    
    public List<Telephone> getTelephones() {
        return telephones;
    }

    public void setTelephones(List<Telephone> telephones) {
        this.telephones = telephones;
    }

    @Override
    public String toString() {
        String tmp ="Contact [prenom=" + prenom + ", nom=" + nom + ", adresse=" + adresse;
        if(telephones !=null) {
            for(Telephone t : telephones) {
                tmp+="  " + t;
            }
        }
        tmp+= "]";
        return tmp;
    }
    
    @PostConstruct
    public void init() {
        System.out.println("Méthode init");
    }
    
    @PreDestroy
    public void destroy() {
        System.out.println("Méthode destroy");
    }
    
}
