package fr.dawan.springcore;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import fr.dawan.springcore.beans.Adresse;
import fr.dawan.springcore.beans.Contact;
import fr.dawan.springcore.beans.Formation;

// Configuration avec java (à priviligier avec spring boot)
@Configuration  // annotation pour indiquer que la classe JavaConfig est une classe de configuration
@Import(AdresseJavaConfig.class) // Import des beans de la classe de configuration AdresseJavaConfig.class
@ComponentScan(basePackages= "fr.dawan.springcore") // choix du package à scanner pour découvrir des composants
public class JavaConfig {
    
    // @Bean ->vDéclaration d'un bean
    // le nom du bean (id,name en xml) = le nom de la méthode, on peut aussi le préciser avec l'attribut name de @Bean
    // La classe (class en xml) du bean correspont au type de retour de la méthode
    @Bean 
    public Formation formation1() {
        return new Formation();
    }
    
    // On peut matérialiser les dépendances avec les paramètres de la méthode
    // ici lien explicite avec le bean qui pour nom adresse2 (équivalent à ref en xml)
    @Bean
    public Contact contact1(Adresse adresse2) {
        return new Contact("John","Doe",adresse2);
    }

}
