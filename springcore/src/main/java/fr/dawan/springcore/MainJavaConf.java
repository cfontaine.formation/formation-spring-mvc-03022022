package fr.dawan.springcore;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import fr.dawan.springcore.beans.Contact;
import fr.dawan.springcore.beans.Formation;
import fr.dawan.springcore.components.HelloWorldComponent;

public class MainJavaConf {

    public static void main(String[] args) {
     // Création du conteneur
        ApplicationContext ctx=new AnnotationConfigApplicationContext(JavaConfig.class);
        System.out.println("---------------------------------------------------");
     // getBean permet de récupérer les instances des beans depuis le conteneur
        Formation f1=ctx.getBean("formation1",Formation.class);
        System.out.println(f1);
        
        HelloWorldComponent hwc=ctx.getBean("helloworld",HelloWorldComponent.class);
        System.out.println(hwc);

        Contact c1=ctx.getBean("contact1",Contact.class);
        System.out.println(c1);
        System.out.println("---------------------------------------------------");
        ((AbstractApplicationContext)ctx).close();
    }

}
