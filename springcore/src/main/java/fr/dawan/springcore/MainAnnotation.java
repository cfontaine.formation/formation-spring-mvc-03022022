package fr.dawan.springcore;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.dawan.springcore.beans.Contact;
import fr.dawan.springcore.components.HelloWorldComponent;

public class MainAnnotation {

    public static void main(String[] args) {
        ApplicationContext ctx=new ClassPathXmlApplicationContext("annotation.xml");
        System.out.println("---------------------------------------------------");
        HelloWorldComponent hw=ctx.getBean("helloworld",HelloWorldComponent.class);
        System.out.println(hw);
        hw.afficher();
        
        HelloWorldComponent hw2=ctx.getBean("helloworld",HelloWorldComponent.class);
        System.out.println(hw2);
        
        Contact c1=ctx.getBean("contact1",Contact.class);
        System.out.println(c1);
        System.out.println("---------------------------------------------------");
        ((AbstractApplicationContext)ctx).close();
    }

}
