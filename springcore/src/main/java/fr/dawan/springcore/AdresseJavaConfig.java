package fr.dawan.springcore;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import fr.dawan.springcore.beans.Adresse;

@Configuration 
public class AdresseJavaConfig {

    @Bean(name= {"adresse2","adresse"} )
    Adresse getAdresse(){
        return new Adresse("1, rue esquermoise","Lille",59800);
    }

}
