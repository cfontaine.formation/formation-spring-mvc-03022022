package fr.dawan.springmvc.forms;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class UserValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {

        return clazz == UserForm.class;
    }

    @Override
    public void validate(Object target, Errors errors) {
        UserForm formUser= (UserForm) target;
        if(formUser.getPassword()==null || formUser.getConfirmPassword()==null || !formUser.getPassword().equals(formUser.getConfirmPassword())){
            formUser.setConfirmPassword("");
            formUser.setPassword("");
            errors.rejectValue("confirmPassword", "user.confirmPassword.notequals","default message");
        }
    }

}
