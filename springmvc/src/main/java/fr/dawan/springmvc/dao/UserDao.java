package fr.dawan.springmvc.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.transaction.annotation.Transactional;

import fr.dawan.springmvc.entities.Article;
import fr.dawan.springmvc.entities.User;

@Transactional
public class UserDao {

    @PersistenceContext
    EntityManager em;

    public void saveOrUpdate(User u) {
        if (u.getId() == 0) {
            em.persist(u);
        } else {
            em.merge(u);
        }
    }

    public void delete(long id) {
        em.remove(em.find(User.class, id));
    }

    public void delete(User u) {
        em.remove(em.contains(u) ? u : em.merge(u));
    }

    @Transactional(readOnly = true)
    public User find(long id) {
        return em.find(User.class, id);
    }

    @Transactional(readOnly = true)
    public List<User> findAll() {
        TypedQuery<User> req = em.createQuery("SELECT u FROM User u", User.class);
        return req.getResultList();
    }

    @Transactional(readOnly = true)
    public User findByEmail(String email, String password) {
        TypedQuery<User> req = em.createQuery("SELECT u FROM User u WHERE u.email=:email AND u.password=:password", User.class);
        req.setParameter("email", email);
        req.setParameter("password", password);
        User u;
        try {
            u = req.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
        return u;
    }

    @Transactional(readOnly = true)
    public User findByEmail(String email) {
        try {
            TypedQuery<User> query = em.createQuery("SELECT u FROM User u WHERE u.email=:email", User.class);
            query.setParameter("email", email);
            return query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

}
