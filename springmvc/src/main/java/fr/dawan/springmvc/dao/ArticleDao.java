package fr.dawan.springmvc.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.transaction.annotation.Transactional;

import fr.dawan.springmvc.entities.Article;

@Transactional
public class ArticleDao {

    @PersistenceContext
    EntityManager em;

    public void saveOrUpdate(Article article) {
        if (article.getId() == 0) {
            em.persist(article);
        } else {
            em.merge(article);
        }
    }

    public void delete(long id) {
        em.remove(em.find(Article.class, id));
    }

    public void delete(Article article) {
        em.remove(em.contains(article) ? article : em.merge(article));
    }

    public Article find(long id) {
        return em.find(Article.class, id);
    }

    public List<Article> findAll() {
        TypedQuery<Article> req = em.createQuery("SELECT a FROM Article a", Article.class);
        return req.getResultList();
    }

}
