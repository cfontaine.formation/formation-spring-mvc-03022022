package fr.dawan.springmvc.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ErrorController {
    @GetMapping("/pageerror")
    public String errorPage(HttpServletRequest request,Model model) {
        int codeError=(int)request.getAttribute("javax.servlet.error.status_code");
        model.addAttribute("codeErreur" ,codeError);
        switch(codeError) {
        case 404:
            model.addAttribute("msgErreur","La page est introuvable");
        break;
        case 500:
            model.addAttribute("msgErreur","Un erreur c'est produite");
         break;
        case 403:
            model.addAttribute("msgErreur","Vous n'avez pas l'autorisation de consulter cette page");
         break;
         default:
               model.addAttribute("msgErreur","Une erreur c'est produite");
        }
        return "erreur";
    }
}
