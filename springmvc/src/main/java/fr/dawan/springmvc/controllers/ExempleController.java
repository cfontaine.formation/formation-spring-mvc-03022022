package fr.dawan.springmvc.controllers;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.inject.Provider;
import javax.mail.Message;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import fr.dawan.springmvc.entities.User;
import freemarker.core.ParseException;
import freemarker.template.Configuration;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.TemplateException;
import freemarker.template.TemplateNotFoundException;

@Controller
@SessionAttributes( {"user2","user3"})
@RequestMapping("/exemple")
public class ExempleController {
    
   private static int cpt;
   
   // Provider session methode 3
   @Autowired
   private Provider<User> userProvider;
   
   // Injection du bean pour l'envoie de mail
   @Autowired
   private JavaMailSender emailsender;
   
   // Pour générer les messages des email avec Freemarker
   @Autowired
   private Configuration configuration;

    
    @GetMapping("")
    public String exemple(@ModelAttribute("cpt") int cpta,Model model) {

        model.addAttribute("msg","Le compteur est égal à "+ cpta  );
        return "exemple";
    }
    
    //@RequestMapping(value="/hello",method = RequestMethod.GET)
    @GetMapping(value= {"/hello","/helloworld"})
    public String helloWorld() {
        return "hello";  // une méthode du controleur retourne le nom de la vue
    }
    
    @GetMapping("/testmodel")
    public String testModel(Model model) {
        int i=42;
        model.addAttribute("i", i);
        model.addAttribute("msg","TestModel");
        return "exemple";
    }
    
    @GetMapping("/testmodelandview")
    public ModelAndView testModelAndView() {    
        ModelAndView mdv =new ModelAndView();
        mdv.setViewName("exemple");
        mdv.addObject("msg", "Test ModelAndView");
        return mdv;
    }
    
    @GetMapping(value="/testparams", params="id=42" )
    public String testParam(Model model) {
        model.addAttribute("msg","La requête contient un paramètre id égal à 42");
        return "exemple";
    }
    
    // @PathVariable
    @GetMapping("testpath/{id}")
    public String testPath(@PathVariable  String id, Model model) {
        model.addAttribute("msg","L'url contient un paramètre id = " + id);
        return "exemple";
    }
    
    @GetMapping("testpath/{id}/action/{action}")
    public String testPathMulti(@PathVariable  String id, @PathVariable String action, Model model) {
        model.addAttribute("msg","L'url contient un paramètre id = " + id +" action=" +action);
        return "exemple";
    }

    
    @RequestMapping(value = "/testpathmap/{id}/action/{action}")
    public String testPathVariableMulti(@PathVariable Map<String, String> map, Model model) { 
        model.addAttribute("msg", "@PathVariable id=" + map.get("id") + "action=" + map.get("action"));
        return "exemple";
    }

    
    @GetMapping("testpathambigue/{id:[0-9]+}")
    public String testPathPb1(@PathVariable  String id, Model model) {
        model.addAttribute("msg","L'url contient un paramètre id = " + id);
        return "exemple";
    }
    
    @GetMapping("testpathambigue/{name:[a-zA-Z]+}")
    public String testPathPb2(@PathVariable  String name, Model model) {
        model.addAttribute("msg","L'url contient un paramètre name = " + name);
        return "exemple";
    }

    @GetMapping(value={"testpathoption/{id}", "testpathoption"})
    public String testPathOptionel(@PathVariable(required = false)  String id, Model model) {
        model.addAttribute("msg","@PathVariable= " + id);
        return "exemple";
    }
    
    // @RequestParam
    @GetMapping("/testparam")
    public String testParam(@RequestParam String id,Model model)
    {
        model.addAttribute("msg","@RequestParam= " + id);
        return "exemple";
    }
    
    @GetMapping("/testparamdefault")
    public String testParamDefault(@RequestParam(defaultValue = "0") String id,Model model)
    {
        model.addAttribute("msg","@RequestParam= " + id);
        return "exemple";
    }
    
    @GetMapping("/testparamconv")
    public String testParam(@RequestParam int val,Model model)
    {
        model.addAttribute("msg","@RequestParam= " + val);
        return "exemple";
    }
    
    @GetMapping("/testparamdate")
    public String testParam(@DateTimeFormat(pattern = "dd-MM-yyyy") @RequestParam LocalDate date,Model model)
    {
        model.addAttribute("msg","@RequestParam= " + date.format(DateTimeFormatter.ofPattern("dd MMMM yy")));
        return "exemple";
    }
    
	
	// @RequestHeader
	@GetMapping("/testheader")
    public String testheader(@RequestHeader("user-agent") String useragent, Model model) {
        model.addAttribute("msg", useragent);
        return "exemple";
    }
	
    @GetMapping("/testallheader")
    public String testAllHeaders(@RequestHeader HttpHeaders headers, Model model) {
        List<String> headParam=new ArrayList<>();
        Set<Entry<String,List<String>>> sent=headers.entrySet();
        for(Entry<String,List<String>> ent: sent)
        {
            String tmp=ent.getKey() + " : ";
            for(String s : ent.getValue()) {
                tmp+= s + " ";
            }
            headParam.add(tmp);
        }
        model.addAttribute("lstHeader", headParam);
        return "exemple";
    }
    
    // Redirection
    @GetMapping("/testredirect")
    public String testRedirect() {
        return "redirect:/exemple/hello";
    }
    
    @GetMapping("/testforward")
    public String testForward() {
        return "forward:/exemple/hello";
    }
    
    // FlashAttribute
    @GetMapping("/testredirectflash")
    public String testFlashAttribute(RedirectAttributes rAtt) {
        rAtt.addFlashAttribute("msgFlash", "Vous venez d'être redirigé");
        return "redirect:/exemple/cibleflash";
    }
    
    @GetMapping("/cibleflash")
    public String cibleFlashAttribute(@ModelAttribute("msgFlash") String msgFlash,Model model) {
            model.addAttribute("msg", msgFlash);
            return "exemple";
    }
    
    
    // @ModelAttribute => méthode toujours appelée avant l'appel de la méthode du controlleur
    // La méthode ajoute un attribut cpt dans le modéle qui prend pour valeur, la valeur retournée par la méthode 
    @ModelAttribute("cpt")
    public int testModelAttribute()
    {
        return cpt++;
    }
    
    
    // Exception
    @GetMapping("/genioexception")
    public void genIoException() throws IOException {
        throw new IOException("IO Exception");
    }
    
    @GetMapping("/gensqlexception")
    public void genSQLException() throws SQLException {
        throw new SQLException("SQL Exception");
    }
    
    @ExceptionHandler(IOException.class)
    public String handlerIOException(Exception e,Model model) {
        model.addAttribute("msgEx", e.getMessage());
        model.addAttribute("trace", e.getStackTrace());
        return "exception"; 
    }
    
    // Cookie
    @GetMapping("/readcookie")
    public String readCookie(@CookieValue(value="testCookie" , defaultValue = "default") String value, Model model){
        model.addAttribute("msg",value);
        return "exemple";
    }
    
    @GetMapping("/writecookie")
    public String writeCookie(HttpServletResponse reponse) {
        Cookie c=new Cookie("testCookie", "Valeur_de_test");
        reponse.addCookie(c);
        return "redirect:/exemple/readcookie";
    }
    
    // Session
    // Méthode 1 : utilisé HttpSession (javaEE)
    @GetMapping("/testsession1")
    public String testSession1(HttpServletRequest request,Model model) {
        HttpSession session=request.getSession();
        session.setAttribute("user1", new User ("John","Doe","jd@dawan.com","123456"));
        model.addAttribute("msg", "Ajout d'un utilisateur dans la session (HttppSession)");
        return"exemple";
    }
    
    @GetMapping("/readsession1")
    public String readSession(HttpServletRequest request,Model model) {
        HttpSession session=request.getSession();
        User u=(User)session.getAttribute("user1");
        if(u!=null) {
            model.addAttribute("msg", u.toString());
        }
        return "exemple";
    }
    
    // Méthode 2 : avec @ModelAttribute et @SessionAttribute
    @GetMapping("/testsession2")
    public ModelAndView testSession2(@ModelAttribute("user2") User user2) {
        ModelAndView model=new ModelAndView("exemple");
        model.addObject("user2",new User ("Jane","Doe","jd@dawan.com","azerty"));
        model.addObject("msg","Ajout d'un utilisateur dans la session (@ModelAttribute et @SessionAttribute)");
        return model;
    }
    
    @GetMapping("/readsession2")
    public String readSession2(@ModelAttribute("user2") User userSession,Model model) {
        model.addAttribute("msg", userSession.toString());
        return "exemple";
    }
    
    @ModelAttribute("user2")
    public User getResquestUser() {
        return new User();
    }
    
    // Méthode 3 : avec une bean Session avec une porté scope
    @GetMapping("/testsession3")
   public String testSession3(Model model) {
        User u=userProvider.get();
        u.setPrenom("Alan");
        u.setNom("Smithee");
        u.setEmail("asmith@dawan.com");
        u.setPassword("azerty_123456");
        model.addAttribute("msg", "Ajout d'un utilisateur dans la session avec une bean Session avec une porté scope");
        return "exemple";
    }
    
    @GetMapping("/readsession3")
    public String readSession3(Model model) {
        User u=userProvider.get();
        if(u!=null) {
            model.addAttribute("msg", u.toString());
        }
        return "exemple";
    }
    
    // Upload avec CommonMultipart
    @PostMapping("/upload")
    public String  uploadFile(@RequestParam("user-file") MultipartFile mpf ,Model model) throws IOException
    {
        String info=mpf.getOriginalFilename();
        info+= " " + mpf.getSize();
        model.addAttribute("msg", info);
        try(BufferedOutputStream bow=new BufferedOutputStream(new FileOutputStream("C:\\Dawan\\"+mpf.getOriginalFilename()))){
            bow.write(mpf.getBytes());
            bow.flush();
        }
        return "exemple";
                
    }
    
    // Vue utilisant freemarker
    @GetMapping("/fremarker")
    public String testFreemarker(Model model) {
        model.addAttribute("msg", "ça marche");
        return "freemarker";
    }
    
    // Envoie d'email text
    @GetMapping("/testmail")
    public String testMail() {
        SimpleMailMessage message=new SimpleMailMessage();
        message.setTo("john.doe@dawan.com");
        message.setFrom("test@dawan.com");
        message.setSubject("Un email de test");
        message.setText("Le contenu du message");
        emailsender.send(message);
        return "redirect:/exemple";
    }
    
    // Envoie d'email html
    @GetMapping("/testmailhtml")
    public String testMailHtml() {
        String msg="<html><body><h1>Le contenu du message</h1></body></html>";
        sendHtmlMail("john.doe@dawan.com","test@dawan.com","Un email de test",msg);
        return "redirect:/exemple";

    }

    // Envoie d'email html avec freemarker
    @GetMapping("/testmailfreemarker")
    public String testMailFreemarker() throws TemplateNotFoundException, MalformedTemplateNameException, ParseException, TemplateException, IOException {
        Map<String,Object> model=new HashMap<>();
        model.put("u", new User("John","Doe","jd@dawan.fr","123456"));
        StringWriter stringWriter = new StringWriter();
        configuration.getTemplate("mailtemplate.ftl").process(model, stringWriter);
        sendHtmlMail("john.doe@dawan.com","test@dawan.com","Un email de test",stringWriter.toString());
        return "redirect:/exemple";
    }
    
    private void sendHtmlMail(String to,String from, String subject, String message) {
        MimeMessagePreparator messagePreparator = new MimeMessagePreparator() {

            @Override
            public void prepare(MimeMessage mimeMessage) throws Exception {
                mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
                mimeMessage.setFrom(from);
                mimeMessage.setSubject(subject,"utf-8");
                 mimeMessage.setContent(message,"text/html");
            }
        };
        emailsender.send(messagePreparator);
    }
     
}
