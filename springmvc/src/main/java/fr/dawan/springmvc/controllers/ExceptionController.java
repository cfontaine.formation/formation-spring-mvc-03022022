package fr.dawan.springmvc.controllers;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionController {
    
    @ExceptionHandler
    public String handlerException(Exception e, Model model) {
        model.addAttribute("msgEx", "Controler Exception " + e.getMessage());
        model.addAttribute("trace", e.getStackTrace());
        return "exception";
    }

}
