package fr.dawan.springmvc.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import fr.dawan.springmvc.dao.UserDao;
import fr.dawan.springmvc.entities.User;
import fr.dawan.springmvc.forms.LoginForm;

@Controller
@SessionAttributes("isConnected")
public class LoginController {
    
    @Autowired
    private UserDao userDao;
    
    @GetMapping("/login")
    public String loginGet(@ModelAttribute("formlogin") LoginForm form) {
        return"login";
    }
    
    @PostMapping("/login")
    public ModelAndView login(@Valid  @ModelAttribute("formlogin") LoginForm form,BindingResult results) {
        ModelAndView mdv= new ModelAndView();
        if(results.hasErrors()) {
            mdv.setViewName("login");
            mdv.addObject("formlogin",form);
            mdv.addObject("errors",results);
        }
        else {
            User u=userDao.findByEmail(form.getEmail(),form.getPassword());
            if(u!=null) {
                mdv.addObject("isConnected", true);
                mdv.setViewName("redirect:/admin/articles");
            }
            else {
                mdv.setViewName("redirect:/login");
            }
        }
        return mdv;
    }
    
    @GetMapping("/logout")
    public String logout(@ModelAttribute ("isConnected") Boolean connected,Model model )
    {
        model.addAttribute("isConnected",false);
        return"redirect:/exemple";
    }

    

    @ModelAttribute("isConnected")
    public boolean initIsConnected() {
        return false;
    }

}
