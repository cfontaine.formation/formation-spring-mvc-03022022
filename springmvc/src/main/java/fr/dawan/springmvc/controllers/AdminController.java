package fr.dawan.springmvc.controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import fr.dawan.springmvc.dao.ArticleDao;
import fr.dawan.springmvc.dao.UserDao;
import fr.dawan.springmvc.entities.Article;
import fr.dawan.springmvc.entities.User;
import fr.dawan.springmvc.forms.ArticleForm;
import fr.dawan.springmvc.forms.UserForm;
import fr.dawan.springmvc.forms.UserValidator;

@Controller
@RequestMapping("/admin")
public class AdminController {
    
    @Autowired
    // private FakeArticleDao daoArticles;
    private ArticleDao articlesDao;
    
    @Autowired
    private UserDao userDao;
    
    @GetMapping("/articles")
    public String displayArticles(Model model) {
        List<Article> lstArt=articlesDao.findAll();
        model.addAttribute("articles",lstArt);
        return "articles";
    }
    
    @GetMapping("/articles/delete/{id}")
    public String deleteArticles(@PathVariable long id) {
        try {
            articlesDao.delete(id);
        } catch (Exception e) {

        }
        return "redirect:/admin/articles";
    }
    
    @GetMapping("/articles/add")
    public String addArticles(@ModelAttribute("formarticle") ArticleForm form) {
        return "articlesadd";
    }
    
    @PostMapping("/articles/add")
    public ModelAndView login(@Valid  @ModelAttribute("formarticle") ArticleForm form,BindingResult results) {
        ModelAndView mdv= new ModelAndView();
        if(results.hasErrors()) {
            mdv.setViewName("articlesadd");
            mdv.addObject("formarticle",form);
            mdv.addObject("errors",results);
        }
        else {
            articlesDao.saveOrUpdate(new Article(form.getDescription(),form.getPrix(),form.getDateAjout(),form.getEmballage()));
            mdv.setViewName("redirect:/admin/articles");
        }
        return mdv;
    }
    
    @GetMapping("/articles/export")
    public String downloadFile(HttpServletResponse response) throws IOException {
        response.setContentType("text/csv");
        response.setHeader("Content-Disposition", "attachment;filename=articles.csv");
        ServletOutputStream o=response.getOutputStream();
        List<Article> lst=articlesDao.findAll();
        for(Article a :lst) {
            String tmp=String.join(";", a.getDescription(),a.getDateAjout().toString(),Double.toString(a.getPrix()),a.getEmballage().toString(),"\n");
            o.write(tmp.getBytes());   
        }
        o.close();
        return "exemple";
    }
    
    
    @GetMapping("/user")
    public String displayUser(Model model) {
        List<User> lstUser=userDao.findAll();
        model.addAttribute("users",lstUser);
        return "user";
    }
    
    @GetMapping("/user/delete/{id}")
    public String deleteUser(@PathVariable long id) {
        try {
            System.out.println("Delete "+ id);
            userDao.delete(id);
        } catch (Exception e) {

        }
        return "redirect:/admin/user";
    }
    
    
    @GetMapping("/user/add")
    public String addUser(@ModelAttribute("formUser") UserForm formUser) {
        return "useradd";
    }

    @PostMapping("/user/add")
    public String addUser(@Valid @ModelAttribute("formUser") UserForm formUser,BindingResult result,Model model) {
       new UserValidator().validate(formUser, result);
        if(result.hasErrors()) {
            model.addAttribute("errors",result);
            model.addAttribute("formuser", formUser);
            return "useradd";
        }
        if(userDao.findByEmail(formUser.getEmail())!=null) {
            model.addAttribute("formuser", new UserForm());
            model.addAttribute("msgerr", "L'utilisateur existe déjà");
            return "useradd";
        }
        
        User user=new User(formUser.getPrenom(),formUser.getNom(),formUser.getEmail(),formUser.getPassword());
        userDao.saveOrUpdate(user);
        return "redirect:/admin/user";
    }

}
