package fr.dawan.springmvc.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import fr.dawan.springmvc.dao.ArticleDao;
import fr.dawan.springmvc.entities.Article;
import fr.dawan.springmvc.entities.BasketLine;

@Controller
@RequestMapping("/articles")
@SessionAttributes("panier")
public class ArticleController {

    @Autowired
    ArticleDao dao;

    @GetMapping("")
    public String getAll(Model model) {

        model.addAttribute("articles", dao.findAll());
        return "store";
    }

    @GetMapping("/basket")
    public String getBasket(@ModelAttribute("panier") List<BasketLine> panier, Model model) {
        model.addAttribute("panier", panier);
        return "basket";
    }
    
    @GetMapping("/basketadd/{id}")
    public String addBasket(@PathVariable long id, @ModelAttribute("panier") List<BasketLine> panier) {

        List<BasketLine> lst = panier.stream().filter(ar -> ar.getArticle().getId() == id).collect(Collectors.toList());
        if (lst != null && lst.size() > 0) {
            lst.forEach(bl -> bl.setQuantity((bl.getQuantity()) + 1));
        } else {
            Article a = dao.find(id);
            if (a != null) {
                panier.add(new BasketLine(a, 1));
            }
        }
        return "redirect:/articles";
    }

    @GetMapping("/basketremove/{id}")
    public String deleteBasket(@PathVariable long id, @ModelAttribute("panier") List<BasketLine> panier) {
        List<BasketLine> lst = panier.stream().filter(ar -> ar.getArticle().getId() == id).collect(Collectors.toList());
        if (lst != null && lst.size() > 0) {
            for(BasketLine bl : lst) {
                panier.remove(bl);
            }
        }
        return "redirect:/articles/basket";
            
    }
    
    
    @GetMapping("/basketinc/{id}")
    public String incQuantityBasketLine(@PathVariable long id, @ModelAttribute("panier") List<BasketLine> panier) {
        List<BasketLine> lst = panier.stream().filter(ar -> ar.getArticle().getId() == id).collect(Collectors.toList());
        if (lst != null && lst.size() > 0) {
            lst.forEach(bl -> bl.setQuantity(bl.getQuantity()+1));
        }
        return "redirect:/articles/basket";
    }
    
    @GetMapping("/basketdec/{id}")
    public String decQuantityBasketLine(@PathVariable long id, @ModelAttribute("panier") List<BasketLine> panier) {
        List<BasketLine> lst = panier.stream().filter(ar -> ar.getArticle().getId() == id).collect(Collectors.toList());
        if (lst != null && lst.size() > 0) {
            lst.forEach(bl -> {
                int q = bl.getQuantity();
                if(q<=1) {
                    panier.remove(bl);
                }
                else {
                    lst.forEach(b -> b.setQuantity(b.getQuantity()-1)); 
                }
            });
        }
        return "redirect:/articles/basket";
    }

    @ModelAttribute("panier")
    public List<BasketLine> initPanier() {
        return new ArrayList<>();
    }
}
