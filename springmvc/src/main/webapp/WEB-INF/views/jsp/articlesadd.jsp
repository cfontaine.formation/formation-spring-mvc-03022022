<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:import url="header.jsp">
    <c:param name="titre" value="Ajout d'articles"/>
</c:import>

<c:url value="/articles/add" context="/springmvc/admin" var="urlAdd"/>
<h1>Ajouter article</h1>
<form:form method="post" action="${urlAdd}" modelAttribute="formarticle">

<div class="mb-3">
<form:label class="form-label" path="description">Description</form:label>
<form:input class="form-control" path="description" type="text" placeholder="Enter la description de l'article"/>
<form:errors class="text-danger small" path="description"/>
</div>

<div class="mb-3">
<form:label class="form-label" path="dateAjout">Date d'ajout</form:label>
<form:input class="form-control" path="dateAjout" type="date"/>
<form:errors class="text-danger small" path="dateAjout"/>
</div>

<div class="mb-3">
<form:label class="form-label" path="emballage">Emballage </form:label>
<form:select class="form-select" path="emballage">
  <form:option value="CARTON">Carton</form:option>
  <form:option value="PLASTIQUE">Plastique</form:option>
  <form:option value="SANS">Sans</form:option>
</form:select>
</div>

<div class="mb-3">
<form:label class="form-label" path="prix">Prix</form:label>
<form:input class="form-control" path="prix" type="number" value="0" step="0.01"/>
<form:errors class="text-danger small" path="prix"/>
</div>

<input class="btn btn-primary" type="submit" value="Ajouter"/>
</form:form>

<c:import url="footer.jsp"/>
