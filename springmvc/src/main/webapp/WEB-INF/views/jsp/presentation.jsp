<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:import url="header.jsp">
    <c:param name="titre" value="Présentation"/>
</c:import>

<h1>Presentation</h1>
<%-- <c:if test="${!empty prenom and !empty nom}"> --%>
<c:if test="${!empty user}"> 
<div class="alert alert-primary col-md-5"><c:out value="Bonjour,${user.prenom} ${user.nom}"/></div>
</c:if>

<div class="my-4">
     <a href='<c:url value="/presentation/path/john/doe" context="/springmvc"/>'>Presentation Path</a>
</div>

<div class="my-4">

<a href='<c:url value="/presentation/param?prenom=Jane&nom=Doe" context="/springmvc" />'/>Presentation Param</a>
</div>

<form method="post" action="<c:url value='/presentation/param' context='/springmvc'/> ">
<div class="">
<div class="mb-3">
  <label for="prenom" class="form-label">Prénom</label>
  <input type="text" class="form-control" id="prenom" name="prenom" placeholder="Entrer votre prénom"/>
</div>
<div class="mb-3">
  <label for="nom" class="form-label">Nom</label>
  <input type="text" class="form-control" id="nom"  name="nom"  placeholder="Entrer votre nom"/>
</div>

 <button class="btn btn-primary  col-md-5" type="submit">Confirmer</button>
  </div>
</form>

 <c:import url="footer.jsp"/>
