<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<c:url value="/webjars/bootstrap/5.1.3/css/bootstrap.min.css" context="/springmvc" var="urlbootstrap"/>
<link rel="stylesheet" href="${urlbootstrap}" />
<title><c:out value=' ${empty param.titre?"Spring mvc":param.titre }'/></title>
</head>
<body>
<nav class='navbar navbar-expand-lg sticky-top mb-4 <spring:theme code='navbar'/>  <spring:theme code='bg'/>'>
    <a class="mx-2 navbar-brand" href="/springmvc/exemple"><spring:message code="brand" text="Formation Spring MVC"/></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="container-fluid">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto">
            <li class="nav-item">
                <a class="nav-link" href='<c:url value="/exemple" context="/springmvc"/>'> <spring:message code="exemple" text="Exemple"/> </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href='<c:url value="/presentation" context="/springmvc"/>'> <spring:message code="presentation" text="Présentation"/></a>
            </li>
            
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLang" role="button" data-bs-toggle="dropdown" aria-expanded="false">
				    <spring:message code="admin" text="Administrateur"/>
				</a>
				<ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLang">
				    <li><a class="dropdown-item" href='<c:url value="/admin/articles" context="/springmvc"/>'>Articles</a></li>
				    <li><a class="dropdown-item" href='<c:url value="/admin/user" context="/springmvc"/>'>Utilisateurs</a></li>
				</ul>
			</li>
            
            <li class="nav-item">
                <a class="nav-link" href='<c:url value="/articles" context="/springmvc"/>'> <spring:message code="articles" text="Articles"/></a>
            </li>      
        
         <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLang" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            <spring:message code="langue" text="Langue"/>
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLang">
            <li><a class="dropdown-item" href="?lang=fr">Français</a></li>
            <li><a class="dropdown-item" href="?lang=en">Anglais</a></li>
            <li><a class="dropdown-item" href="?lang=es">Espagnol</a></li>
          </ul>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuTheme" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            <spring:message code="theme" text="Thème"/>
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuTheme">
            <li><a class="dropdown-item" href="?theme=default">Sombre</a></li>
            <li><a class="dropdown-item" href="?theme=light">Claire</a></li>
            <li><a class="dropdown-item" href="?theme=blue">Bleu</a></li>
          </ul>
        </li>
        </ul>
        <div >
        
        </div>
        <a class="<spring:theme code='basket'/> text-decoration-none  mx-5" href="<c:url value='/basket' context='/springmvc/articles'/>" ><spring:message code="basket" text="Panier"/></a>
      <c:choose>
      <c:when test='${empty sessionScope.isConnected ||  not sessionScope.isConnected}'>
       <a class="btn <spring:theme code='login'/> btn-sm" href="<c:url value='/login' context='/springmvc'/>" ><spring:message code="login" text="Connexion"/></a>
      </c:when>
      <c:otherwise>
          <a class="btn <spring:theme code='logout'/> btn-sm" href="<c:url value='/logout' context='/springmvc'/>" ><spring:message code="logout" text="Se déconnecter"/></a>
      </c:otherwise>
      </c:choose>
        </div>
    </div>
</nav>
<main class="container-fluid">

