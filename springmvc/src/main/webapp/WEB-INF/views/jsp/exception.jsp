<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:import url="header.jsp">
    <c:param name="titre" value="Exception"/>
</c:import>

<h1 class="mx-5  my-5"><c:out value="${msgEx}"/></h1>
<c:forEach items="${trace}" var="elm">
<div class="mx-5 text-danger"><c:out value="${elm}"/></div>
</c:forEach>


<c:import url="footer.jsp"/>