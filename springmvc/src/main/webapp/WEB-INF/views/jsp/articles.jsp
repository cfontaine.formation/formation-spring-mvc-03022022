<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:import url="header.jsp">
    <c:param name="titre" value="Administration des articles"/>
</c:import>
<h1 class="mb-3">Liste des articles</h1>
<a class="btn btn-sm btn-outline-secondary" href='<c:url value="/articles/add" context="/springmvc/admin"/>'/>Ajouter articles</a>
<a class="btn btn-sm btn-outline-secondary mx-3" href='<c:url value="/articles/export" context="/springmvc/admin"/>'/>Export .csv</a>
</div>
<table class="table table-hover table-responsive-sm mt-3">
    <thead>
        <tr>
            <th scope="col">id</th>
            <th scope="col">description</th>
            <th scope="col">prix</th>
            <th scope="col">date ajout</th>
            <th scope="col">emballage</th>
            <th scope="col">action</th>
        </tr>
    </thead>
    <tbody>
    <c:forEach items="${articles}" var="article">
    <tr>
        <th scope="row">${article.id}</th>
        <td><c:out value ='${article.description}'/></td>
        <td><c:out value ='${article.prix}'/></td>
        <td><c:out value ='${article.dateAjout}'/></td>
        <td><c:out value ='${article.emballage}'/></td>
        <td><a class="btn btn-danger btn-sm" href="<c:url value='/articles/delete/${article.id}' context='/springmvc/admin'/>"> supprimer</a> </td>
    </tr>
    </c:forEach>
    
    </tbody>
</table>
<c:import url="footer.jsp"/>