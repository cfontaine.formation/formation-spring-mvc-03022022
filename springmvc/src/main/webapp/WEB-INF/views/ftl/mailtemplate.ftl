<!DOCTYPE html>
<html lang="fr">
<head>
    <link rel="stylesheet" href='<c:url value="/webjars/bootstrap/5.1.3/css/bootstrap.min.css" context="/springmvc"/>' />
</head>
<body>
<p>,</p>
<p>Une demande de renouvellement de mot de passe a été faite pour votre compte.</p>
<p>Réinitialiser le mot de passe en utilisant le lien suivant : </p>
<p><a class="btn btn-primary" href="/springmvc/exemple Le mot de passe est réinitialisé'">réinitialiser votre mot de passe </a></p>
<p>Cordialement</p>
</body>
</html>