<!DOCTYPE html>
<html lang="fr">
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>
<p>${u.prenom},
<p>Une demande de renouvellement de mot de passe a été faite pour votre compte: <strong>${u.email}</strong>.
<p>Réinitialiser le mot de passe en utilisant le lien suivant : </p>
<p><a class="btn btn-primary" href="/springmvc/exemple">réinitialiser votre mot de passe </a></p>
<p>Cordialement</p>
</body>
</html>